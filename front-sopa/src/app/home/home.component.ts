import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SopaModel } from '../models/sopa-model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  sopaForm !: FormGroup;
  cargando : boolean = false;
  sopaModel : SopaModel = new SopaModel();

  constructor(
    private fb: FormBuilder
  ) {
    this.crearFormulario();
   }

  ngOnInit(): void {
  }

  limpiar() {
    this.cargando = false;
    this.sopaForm.reset()
  }

  resolver() {
    this.cargando = true;
    this.sopaModel.ren      = this.sopaForm.get('ren')?.value;
    this.sopaModel.col      = this.sopaForm.get('col')?.value;
    this.sopaModel.palabras = this.sopaForm.get('palabras')?.value;
    this.sopaModel.sopa     = this.sopaForm.get('sopa')?.value;
    console.log( this.sopaModel );

    /* Después de encontrar la solución*/
    //this.cargando = false;
  }

  crearFormulario() {
    this.sopaForm = this.fb.group({
      ren      : ['', [ Validators.required, Validators.pattern('[0-9]*'), Validators.min(5) ]],
      col      : ['', [ Validators.required, Validators.pattern('[0-9]*'), Validators.min(5) ]],
      palabras : ['', [ Validators.required, Validators.pattern('[A-Z|A-Z,]*') ]],
      sopa     : ['', [ Validators.required, Validators.pattern('[A-Z|A-Z\n]*') ]
      ]
    });
  }

}