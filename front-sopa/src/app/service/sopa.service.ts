// *********** Librerias de angular ***************
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// *********** Modelos ***************

import { SopaModel } from '../models/sopa-model'; 

@Injectable({
  providedIn: 'root'
})
export class SopaService {

  private url = 'http://localhost:4000';

  constructor( private http:HttpClient ) { 
  }

  
  getSopa( ) {
    return this.http.get( `${ this.url }/` );
  }

  nuevaSopa( sopa: SopaModel ) {
    const Data = {
      ...sopa,
    }
    return this.http.post( `${this.url}`, Data );
  }
}