import numbers

def obtenerDatos():
    archivo=open("prueba.txt","r")
    datos=archivo.readlines()
    M,N=(datos[0].replace("\n","")).split(" ")
    matriz=[]
    for i in range (0,int(M)):
        columna=[]
        for j in range (0,int(N)):
            columna.append(datos[1+i][j])
        matriz.append(columna)
    palabras=datos[-1].split(",")
    return int(M),int(N),matriz,palabras

def verificando( op, i, j, matriz, palabra ):
    if len(palabra)==1:
        respuesta=True  
        return respuesta
    else:
        if (op==1):     #manera 1:"hacia la derecha"
            i=i
            j=j+1
        elif (op==2):   #manera 2:"hacia la izquierda"
            i=i
            j=j-1
        elif (op==3):   #manera 3:"hacia arriba"
            i=i-1
            j=j
        elif (op==4):   #manera 4:"hacia abajo"
            i=i+1
            j=j
        elif (op==5):   #manera 5:"diagonal Superior Izquierda"
            i=i-1
            j=j-1
        elif (op==6):   #manera 6:"diagonal Inferior Izquierda"
            i=i+1
            j=j-1
        elif (op==7):   #manera 7:"diagonal Superior Derecha"
            i=i-1
            j=j+1
        elif (op==8):   #manera 8:"diagonal Inferior Derecha"
            i=i+1
            j=j+1
        else:
            return False
            print( "Error [+]")

        try:
            if matriz[i][j] == palabra[0]:
                nueva = palabra[1:]
                a = verificando( op,i,j,matriz,nueva )
                if a==True:
                    return a
            else:
                respuesta=False
                return respuesta
        except:
            respuesta=False
            return respuesta

def posibilidades(matriz,i,j,palabra):
    solucion={
        "palabra": '',
        "ren": 0,
        "col": 0,
        "direccion": ''
    }
    opciones={
        1:"hacia la derecha", 
        2:"hacia la izquierda", 
        3:"hacia arriba", 
        4:"hacia abajo",
        5:"diagonal Superior Izquierda", 
        6:"diagonal Inferior Izquierda", 
        7:"diagonal Superior Derecha",
        8:"diagonal Inferior Derecha"
    }
    nueva=palabra[1:]

    for o in range(1,9):
        verifi=verificando(o,i,j,matriz,nueva)
        if (verifi==True):
            solucion["palabra"] = palabra
            solucion["ren"] = i+1
            solucion["col"] = j+1
            solucion["direccion"] = opciones[o]
            print ("Palabra: {0}, Ubicacion: ({1},{2}), Direccion: {3}").format(solucion["palabra"],solucion["ren"],solucion["col"],solucion["direccion"])
            return verifi

def posicionInicial(buscando,matriz,M,N):
    for i in range(0,M):
        for j in range(0,N):
            if (buscando[0] == matriz[i][j]):
                terminado=posibilidades(matriz,i,j,buscando)
                if (terminado==True):
                    return terminado
    
M,N,matriz,palabras=obtenerDatos()
for p in palabras:
    posicionInicial(p,matriz,M,N)